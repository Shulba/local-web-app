const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const CopyPlugin = require('copy-webpack-plugin');

let config = {
    entry: "./src/js/DisplayModule.js",
    output: {
        path: path.resolve(__dirname, "./dist"),
        library: 'NameLibrary',
        filename: "bundle.js",
       //publicPath: "dist/"
    },
    module:{
        rules:[{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: "babel-loader"
        }, {
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
                //fallback: "style-loader",
                use: "css-loader"
            })
        }, {
            test: /\.html$/,
            use: ['html-loader']
        }, {
            test: /\.(jpg|png)$/,
            use: [{
                loader: 'file-loader',
                options:{
                    name: '[path][name].[ext]',
                    useRelativePath: true,
                    outputPath: 'img/',
                    publicPath: 'img/'
                }
            }]
        }]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html'
        }),
        new ExtractTextPlugin("styles.css"),
        new CleanWebpackPlugin("[dist]"),
        new CopyPlugin([
            { from: './src/img', to: './img' },
          ])
    ],
    devServer:{
       /* contentBase: path.resolve(__dirname, "./src"),
        port: 3000,*/
        overlay: true
    }
}
//module.exports = config;
module.exports = (env, options) =>{
    let prod = options.mode === "production";
    config.devtool = prod
                ? false
                : 'eval-sourcemap';
    return config
}