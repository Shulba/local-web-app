import { personStatus } from './PlaceModule';

export class TakeName{
		constructor(firstWord, lastWord){
			this.firstWord = firstWord;
			this.lastWord = lastWord;
		}
		fullPrase() {
			return `${this.firstWord} ${this.lastWord}`;
		}
}
