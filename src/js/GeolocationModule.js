const geolocation = require('geolocation');
const mapboxgl = require('mapbox-gl');

export let map = {};
let longitude = 0;
let latitude = 0;

export function geoPosition(){
  geolocation.getCurrentPosition(function (err, position) {
    if(err) throw err
    longitude = position.coords.longitude;
    latitude = position.coords.latitude;
    generateMa()
  })
}
window.onload = function(){
  geoPosition();
}

function generateMa(){
  mapboxgl.accessToken = 'pk.eyJ1IjoiemVub3ZpeSIsImEiOiJjanN2dGZ1aXgwYW5sNDRvNGJ5aXprd2xmIn0.Ybno_fhnTvieyG9H-yzNHQ';
  map = new mapboxgl.Map({
    container: 'map-box',
    style: 'mapbox://styles/zenoviy/cjsvtogqh634w1fpagejidy6v',
    center: [longitude, latitude],
    zoom: 12
  });
  placeMarker(longitude, latitude,{personeName:"You location"})
}
export function placeMarker(long, lat, object){
  
  var geojson = {
    type: 'FeatureCollection',
    features: [{
      id: "symbols",
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [long, lat]
      },
      properties: {
        title: `${object.personeName}`,
        description: `Long: ${long.toFixed(4)},<br> Lat: ${lat.toFixed(4)}`
      }
    }]
  }
  // add markers to map
  geojson.features.forEach(function(marker) {
    var el = document.createElement('div');
    el.setAttribute('id', 'symbols');
    el.className = 'marker';
    console.log(object);
    (object.animalIcon)
    ?el.style = `background-image: url('../img/${object.animalIcon}')`
    :false;

    let newMarker = new mapboxgl.Marker(el)
      .setLngLat(marker.geometry.coordinates)
      .setPopup(new mapboxgl.Popup({ offset: 25 }) // add popups
      .setHTML('<h3>' + marker.properties.title + '</h3><p>' + marker.properties.description + '</p>'))
      .addTo(map);

      object.mapPosition = [long.toFixed(4), lat.toFixed(4)]
        map.flyTo({center: object.mapPosition,
          zoom: 12});
      (object)? object.mapMarker = newMarker: false;
      
  });
}