import {objectsArr, currentObject} from './UIOperationModule'
export class FeedTheAnimal{
    constructor(id){
        this.startRate;
        this.procentagePerTimeRate;
        this.feedPerClick;
        this.id = id;
        this.state;
        this.button;
        this.colorArr = ['red', '#ffb300', '#4dc84d'];
        this.color;
    }
    entryFeedProcess(){
        this.startRate = Math.floor(Math.random()*40+20);
        this.feedPerClick = Math.floor(Math.random()*10+1);
    }
    createButton(){
        this.button = `<input id="feed-button-${this.id}" type="button" value="Feed the animal">`
        return this.button
    }
    addFoodProcess(object){
        this.startRate += this.feedPerClick;
        if(this.startRate>100){
            this.startRate = 100;
        }
        this.progressColor()
        this.changeBar(object)
    }
    changeBar(objectSelector){
        let changebleArea= document.querySelector(objectSelector);
        changebleArea.style.width = `${this.startRate}%`;
        changebleArea.innerText = `${this.startRate}%`;
        changebleArea.style["background-color"] = this.color;
    }
    progressColor(){
        if(this.startRate<=30){
            this.color = this.colorArr[0];
        }else if(this.startRate>30&&this.startRate<=60){
            this.color = this.colorArr[1];
        }else{
            this.color = this.colorArr[2];
        }
    }
    hungryProcess(){
        this.startRate -= this.feedPerClick;
        if(this.startRate<0){
            this.startRate = 0;
        }
    }
    createProgressBar(){
        this.progressColor()
        let progressBar = `<div class="progress-wrapper">
            <div id="inner-bar" style="width:${this.startRate}%; background-color:${this.color};" class="inner-bar">${this.startRate}%</div> 
        </div>  `
        return progressBar
    }
}

let interval = setInterval(function(){
    if(objectsArr.length>0){
        animalLivingProcess();
    }
}, 60000)
function animalLivingProcess(){
        objectsArr.forEach((propertis)=>{
            propertis.feedProcesor.hungryProcess();
            if(propertis === currentObject){
                propertis.feedProcesor.progressColor();
                propertis.feedProcesor.changeBar("#inner-bar");
            }
        })
}