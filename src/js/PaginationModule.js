import {showAllRows, objectsArr, displayArea, listEvents, paginationObject} from "./UIOperationModule"
export class PaginationObjet {
    constructor(){
        this.totalItems;
        this.currentPage = 1;
        this.pageSize = 3;
        this.totalPages;    // number of pages
        this.paginationWrapper;
        this.paginationStatus = false;
        this.buttonObject = [];

        this.firstItem= 0;
        this.lastItem = this.pageSize;
    }
    defineTotalPages(objectsArr, displayArea){        // calculate total pages
        this.totalItems = objectsArr.length;
        this.totalPages = Math.ceil(this.totalItems/ this.pageSize);
        if(this.totalPages>1){
            this.drawPaginationButtons(displayArea);
            this.paginationStatus = true;
        }else{
            this.paginationStatus = false;
        }
    }
    drawPaginationButtons(displayArea){
        this.rowCreator(displayArea, "main", "pag-wrapper-", false, "paginate-wrapper");
        this.paginationWrapper = document.querySelector("#pag-wrapper-main");
        for(let i = 0; i< this.totalPages; i++){
            this.rowCreator(this.paginationWrapper, i, "pag-button-", true, "paginate-button")
        }
        this.buttonObject = [...document.getElementsByClassName("paginate-button")];
        this.buttonObject.forEach((x)=>{
            x.onclick = function(){
                paginationObject.erresButtonStyle();
                paginationObject.currentPage = parseInt(x.innerText);
                paginationObject.chosePage();
                this.className = "paginate-button selected";
                if(paginationObject.buttonObject&&paginationObject.buttonObject.length>0){
                    paginationObject.buttonObject[paginationObject.currentPage-1].className = "paginate-button selected";
                }
            }
        })
    }
    chosePage(){
        if((this.currentPage>1)&&(this.currentPage!=this.totalPages)){
            this.firstItem = this.currentPage*this.pageSize-this.pageSize;
            this.lastItem = this.firstItem+this.pageSize;
        }else if(this.currentPage==this.totalPages){
            this.firstItem = this.currentPage*this.pageSize-this.pageSize;
            this.lastItem = this.totalItems;
        }else if(this.currentPage==1){
            this.firstItem = 0;
            this.lastItem = this.pageSize;
        }
        showAllRows(displayArea, objectsArr.slice(this.firstItem, this.lastItem));
        listEvents("erresse-block");
        listEvents("single-box","info-block");
    }
    erresButtonStyle(){
        this.buttonObject.forEach((x)=>{
            x.className = "paginate-button";
        })
    }
    rowCreator(whereToDisplay, identi, selectorName, paginationState, className){
        let newObject = document.createElement("div");
        newObject.setAttribute("class", className);
        newObject.setAttribute("id", `${selectorName}${identi}`);
        whereToDisplay.append(newObject);
        if(paginationState){
            document.querySelector(`#${selectorName}${identi}`).textContent = identi+1;
        }
    }
}