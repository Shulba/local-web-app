import { DefinePerson } from './PersoneModule';

export let personeStatus = new DefinePerson();

export class FindMyPlace{
	constructor(){
		this.personPlace = "Home";
		this.livingPlace = ["River/lake", "City/Streets/Near Home", "Woods/Mountains", "Sky/Tree", "Ocean"];
	}
	findPlace(){
		personeStatus.findYourPersone();
		let name = personeStatus.animal.split(' ');
		name = name[0];
		if(name == 'beaver'){
			this.personPlace = this.livingPlace[0];
		}else if(name == 'dog'||name == 'cat'){
			this.personPlace = this.livingPlace[1];
		}else if(name == 'bear'){
			this.personPlace = this.livingPlace[2];
		}else if(name == 'eagle'){
			this.personPlace = this.livingPlace[3];
		}else if(name == 'dolphin'){
			this.personPlace = this.livingPlace[4];
		}
		return this.personPlace
	}
}



