import {PaginationObjet} from './PaginationModule';
import {placeMarker, map} from './GeolocationModule';

let displayBlock = document.querySelector("#display-block");
let input1 = document.querySelector("#enter-field-1");
let input2 = document.querySelector("#enter-field-2");
let id = 0;
let alertMessage = false;
let filteredState = false;
let filteredArr = [];
let allbuttons;
export let displayArea = document.querySelector("#info-block");
export let input3 = document.querySelector("#enter-field-3");
export let input4 = document.querySelector("#input-4");
export let objectsArr = [];
export let currentObject;

//;
function removeButtonsClass(obj){
    obj.forEach((x)=>{
        x.className = "single-box";
    })
    removeButtonState()
}
function removeButtonState(){
    objectsArr.forEach((x)=>{
        x.selected = false;
    })
}
export let paginationObject = new PaginationObjet();
    export function showResult(){
        if(input1.value != ''&&input2.value != ''){
            //var chattBot = new Erp.StartChatting(input1.value, input2.value, id);
            var chattBot = new NameLibrary.StartChatting(input1.value, input2.value, id);

            filteredState = false;
            objectsArr.push(chattBot);
            createSingleRow(id, displayArea);
            paginationObject.currentPage = paginationObject.totalPages;
            showAllRows(displayArea, objectsArr.slice(paginationObject.totalPages*paginationObject.pageSize-paginationObject.pageSize, paginationObject.totalItems));
            document.querySelector(`#box-${id}`).innerHTML = chattBot.createConversation(objectsArr.indexOf(chattBot));
            placeMarker(Math.random()*1 + Math.random()*1+23, Math.random()*0.2 + 49.7, chattBot);
            listEvents("erresse-block");
            listEvents("single-box","info-block");
            input1.value = '';
            input2.value = '';
            id++
            if(alertMessage){
                displayBlock.innerHTML = '';
                alertMessage = !alertMessage;
            }
        }else{
            alertMessage = !alertMessage;
            displayBlock.innerHTML = `<div class="info-alert">Please enter You name!</div>`
            removeButtonState();
            if(allbuttons.length>0){
                removeButtonsClass(allbuttons);
            }
        }
    }
    export function showAllRows(displayArea, nameArray){
        displayArea.innerHTML = '';
        if(nameArray.length>0){
            nameArray.forEach((x)=>{
                createSingleRow(x.objectId, displayArea);
                let button = document.querySelector(`#box-${x.objectId}`);
                (x.selected)
                ? button.className = "single-box selected"
                : button.className = "single-box";
                button.innerHTML = x.createSingleRow(objectsArr.indexOf(x));
            })
        }
        if(paginationObject.buttonObject&&paginationObject.buttonObject.length>0){
            paginationObject.buttonObject[paginationObject.currentPage-1].className = "paginate-button selected";
        }
    }
    export function createSingleRow(id, displayArea){
        let newObject = document.createElement('li');
        newObject.setAttribute(`id`, `box-${id}`);
        newObject.setAttribute(`class`, `single-box`);
        newObject.setAttribute(`title`, `${id}`);
        displayArea.append(newObject);
        if(paginationObject.paginationWrapper){
            paginationObject.paginationWrapper.remove();
        }
        if(filteredState==false){
            paginationObject.defineTotalPages(objectsArr, displayArea);
        }else{
            paginationObject.defineTotalPages(filteredArr, displayArea);
        }
    }
    export function listEvents(classList){
        allbuttons = document.getElementsByClassName(classList);
        allbuttons = [...allbuttons];
        allbuttons.forEach((x) =>{
            x.onclick = function(){
                if(x.localName=="span"){
                    clearBlock.call(this.parentElement);
                }else if(x.localName=="li"){
                    removeButtonsClass(allbuttons);
                    x.className = "single-box selected";
                    showInfo.call(this);
                }
            }
        })
    }
    export function showInfo(){
        objectsArr.find((x, i) =>{
            if(this.title == x.objectId){
                currentObject = x;
                x.selected = true;
                console.log(map, 'map object')
                showEventOnMap(x);
                let addInfo = `<p><label>First name:</label> <span>${x.firstName}</span></p>
                <p><label>Last name:</label> <span>${x.lastName}</span></p>
                <p><label>You animal is:</label> <span>${x.personeName}</span></p>
                <p><label>Live at:</label> <span>${x.place}</span></p>
                <div class="row">
                    <div class="column">
                        <img src='./img/${x.image}'>
                        <div id="game-wrapper" class="game-wrapper"></div>
                    </div>
                    <div class="column">
                        <h1>${x.personeName}</h1>
                        <h2>${x.personPhrase}</h2>
                        <p>${x.descriptionText}</p>
                    </div>

                </div>
                `;
                displayBlock.innerHTML = addInfo;
                let parrentGameObject = document.querySelector("#game-wrapper");
                createGameArea(x, parrentGameObject)
                document.querySelector(`#feed-button-${x.objectId}`).onclick = function(){
                    x.feedProcesor.addFoodProcess("#inner-bar");
                }
                return this.parentElement.title == x.objectId
            }
        });
    }
    function showEventOnMap(object){
        map.flyTo({center: object.mapPosition,
            zoom: 12
        });
    }
    function createGameArea(object, parrent){
                let gameAreaField = `<div class="controller-wrapper">
                    ${object.feedProcesor.createProgressBar()}
                    <div class="button-wrapper">${object.feedProcesor.createButton()}</div>
                </div>`
                parrent.innerHTML = gameAreaField;

    }
    export function clearBlock(){       //delateElements
        objectsArr.find((x, i) => {
            if(this.title == x.objectId){
                currentObject = {};
                displayBlock.innerHTML = `<div class="info-alert">You remove: ${x.firstName} ${x.lastName} ${x.personeName} #${x.objectId}</div>`;
                //console.log(x.mapMarker)
                x.mapMarker._element.remove();
                objectsArr.splice(i, 1);
                this.remove();
                if(filteredState==false){
                    paginationObject.defineTotalPages(objectsArr, displayArea);
                }else{
                    paginationObject.defineTotalPages(filteredArr, displayArea);
                }
                showAllRows(displayArea, objectsArr.slice(paginationObject.totalPages*paginationObject.pageSize-paginationObject.pageSize, paginationObject.totalItems));
                listEvents("erresse-block");
                listEvents("single-box","info-block");
                return this.title == x.objectId
            }
        });
    }
    export function searchForm(name){
        currentObject = {};
       filteredArr = objectsArr.filter((x)=>{
            return x.firstName.indexOf(name)>-1
        })
        if(filteredArr.length>0){
            filteredState = true;
            showAllRows(displayArea, filteredArr.slice(0, paginationObject.lastItem));
        }else{
            filteredState = false;
            showAllRows(displayArea, []);
        }
        listEvents("erresse-block");
        listEvents("single-box","info-block");
        removeButtonsClass(allbuttons);
        displayBlock.innerHTML = "";
    }
