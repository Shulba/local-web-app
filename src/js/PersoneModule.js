export class DefinePerson{
		constructor(){
			this.animal = 'beaver';
			this.phrase;
			this.personeTypeArr = ['beaver', 'dog', 'cat', 'bear', 'eagle', 'dolphin'];
			this.description;
			this.image;
			this.foodCoef;
			this.speedCoef;
			this.animalIcon;
		}
		findYourPersone(){
			let randomizer = Math.floor(Math.random()*this.personeTypeArr.length);
			let person = this.personeTypeArr[randomizer];
			this.animal = person;

			this.phrase = animalPhrases[person].phrase;
			this.description = animalPhrases[person].text;
			this.image = animalPhrases[person].image;

			this.foodCoef = animalPhrases[person].foodCoef;
			this.speedCoef = animalPhrases[person].speedCoef;
			this.animalIcon = animalPhrases[person].icon;
			return this.personName
		}
}

const animalPhrases = {
	beaver: {
		phrase:' GET to the chopper!',
		text: `The beaver (genus Castor) is a large, primarily nocturnal, semiaquatic rodent. 
		Castor includes two extant species, the North American beaver (Castor canadensis) 
		(native to North America) and Eurasian beaver (Castor fiber) (Eurasia). 
		Beavers are known for building dams, canals, and lodges (homes). 
		They are the second-largest rodent in the world (after the capybara). 
		Their colonies create one or more dams to provide still, deep water to 
		protect against predators, and to float food and building material. The 
		North American beaver population was once more than 60 million, but as of 1988 
		was 6–12 million. This population decline is the result of extensive hunting for fur, 
		for glands used as medicine and perfume, and because the beavers' harvesting of trees 
		and flooding of waterways may interfere with other land uses. `,
		image: 'image.jpg',
		icon: 'beaver-icon.png',
		foodCoef: 10,
		speedCoef: 4,
		location: [[
			22.8955078125,
          50.958426723359935
		  ],[
			35.04638671874999,
          51.11041991029264
		  ],[
			35.52978515624999,
          46.649436163350245
		  ],[
			22.8076171875,
          46.5739667965278
		  ],
		]
	},
	dog: {
		phrase:' go fetch somethisng!',
		text: `The domestic dog (Canis lupus familiaris when considered a subspecies of the wolf or Canis familiaris when considered a distinct species)
		 is a member of the genus Canis (canines), which forms part of the wolf-like canids,
		  and is the most widely abundant terrestrial carnivore.
		   The dog and the extant gray wolf are sister taxa as modern wolves are not closely 
		   related to the wolves that were first domesticated, which implies that the direct 
		   ancestor of the dog is extinct. The dog was the first species to be domesticated 
		   and has been selectively bred over millennia for various behaviors, sensory capabilities,
		    and physical attributes. `,
		image: 'dog.jpg',
		icon: 'dog-icon.png',
		foodCoef: 5,
		speedCoef: 4,
		location: [[
			22.8955078125,
          50.958426723359935
		  ],[
			35.04638671874999,
          51.11041991029264
		  ],[
			35.52978515624999,
          46.649436163350245
		  ],[
			22.8076171875,
          46.5739667965278
		  ],
		]
	},
	cat: {
		phrase:' lazy animal!',
		text: `The cat or domestic cat (Felis catus) is a small carnivorous mammal. 
		It is the only domesticated species in the family Felidae. The cat is either a house cat, 
		kept as a pet, or a feral cat, freely ranging and avoiding human contact. A house cat is 
		valued by humans for companionship and for its ability to hunt rodents. About 60 cat breeds
		 are recognized by various cat registries. `,
		image: 'cat.jpg',
		icon: 'cat-icon.png',
		foodCoef: 5,
		speedCoef: 4,
		location: [[
			22.8955078125,
          50.958426723359935
		  ],[
			35.04638671874999,
          51.11041991029264
		  ],[
			35.52978515624999,
          46.649436163350245
		  ],[
			22.8076171875,
          46.5739667965278
		  ],
		]
	},
	bear: {
		phrase:' Big and Fluffy',
		text: `Bears are carnivoran mammals of the family Ursidae. They are classified as caniforms, 
		or doglike carnivorans. Although only eight species of bears are extant, they are widespread, 
		appearing in a wide variety of habitats throughout the Northern Hemisphere and partially in 
		the Southern Hemisphere. Bears are found on the continents of North America, South America,
		 Europe, and Asia. Common characteristics of modern bears include large bodies with stocky 
		 legs, long snouts, small rounded ears, shaggy hair, plantigrade paws with five nonretractile 
		 claws, and short tails. `,
		 image: 'bear1.jpg',
		 icon: 'bear-icon.png',
		 foodCoef: 5,
		 speedCoef: 4,
		 location: [[
			 22.8955078125,
		   50.958426723359935
		   ],[
			 35.04638671874999,
		   51.11041991029264
		   ],[
			 35.52978515624999,
		   46.649436163350245
		   ],[
			 22.8076171875,
		   46.5739667965278
		   ],
		 ]
	},
	eagle: {
		phrase:' Fly to the sky',
		text: `Eagle is the common name for many 
		large birds of prey of the family Accipitridae. 
		Eagles belong to several groups of genera, not 
		all of which are closely related. Most of the 60 
		species of eagle are from Eurasia and Africa. Outside this area, 
		just 14 species can be found—2 in North America, 9 in Central and South America, 
		and 3 in Australia. `,
		image: 'eagle.jpg',
		icon: 'eagle-icon.png',
		foodCoef: 5,
		speedCoef: 4,
		location: [[
			22.8955078125,
          50.958426723359935
		  ],[
			35.04638671874999,
          51.11041991029264
		  ],[
			35.52978515624999,
          46.649436163350245
		  ],[
			22.8076171875,
          46.5739667965278
		  ],
		]
	},
	dolphin: {
		phrase:' wont some fish?',
		text: `Dolphin is a common name of aquatic mammals within the order Cetacea, 
		arbitrarily excluding whales and porpoises. The term dolphin usually refers 
		to the extant families Delphinidae (the oceanic dolphins), Platanistidae 
		(the Indian river dolphins), Iniidae (the new world river dolphins), and Pontoporiidae 
		(the brackish dolphins), and the extinct Lipotidae (baiji or Chinese river dolphin). 
		There are 40 extant species named as dolphins.  `,
		image: 'dolphin.jpg',
		foodCoef: 5,
		speedCoef: 4,
		location: [[
			-64.51171875,
          12.726084296948196
		  ],[
			-63.80859374999999,
          43.45291889355465
		  ],[
			-13.0078125,
          42.8115217450979
		  ],[
			-12.3046875,
          10.833305983642491
		  ],
		]
	}
}