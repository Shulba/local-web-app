
import { FindMyPlace, personeStatus } from './PlaceModule';
import { TakeName } from './PhraseModule';
import { showResult, searchForm, input3, input4 } from './UIOperationModule';
import { FeedTheAnimal } from './FeedModule'

export class StartChatting{
	constructor(firstName, lastName, id, ){
		this.firstName = firstName;
		this.lastName = lastName;
		this.objectId = id;
		this.fullPhrase;
		this.personPhrase;
		this.personeName;
		this.place;
		this.erres;
		this.descriptionText;
		this.image;
		this.animalIcon;
		this.selected = false;
		this.feedProcesor = new FeedTheAnimal(id);
	}
	createConversation(index){
		let placeDefine = new FindMyPlace();
		this.feedProcesor.entryFeedProcess();
		placeDefine.findPlace();
		let phrase = new TakeName(this.firstName, this.lastName);
		this.erres =  `<span class="erresse-block">
		<svg id="Layer_1" data-name="Layer 1" width="30" height="30" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496.16 496.15">
			<defs>
				<style>
					.cls-1{fill:#e04f5f;}
					.cls-2{fill:#fff;}
				</style>
			</defs>
			<title>cancel</title>
			<path class="cls-1" d="M496.16,248.09C496.16,111.06,385.09,0,248.08,0S0,111.06,0,248.09,111.07,496.16,248.08,496.16,496.16,385.09,496.16,248.09Z" transform="translate(0 0)"/>
			<path class="cls-2" d="M277,248.08l72.53-84.2a21.95,21.95,0,0,0-33.26-28.65l-68.24,79.21-68.23-79.21a21.95,21.95,0,1,0-33.26,28.65l72.52,84.2-72.52,84.19a21.95,21.95,0,0,0,33.26,28.64l68.23-79.21,68.24,79.21a21.95,21.95,0,0,0,33.26-28.64Z" transform="translate(0 0)"/>
			</svg>
		</span>`;
		this.fullPhrase = phrase.fullPrase();
		this.personPhrase = personeStatus.phrase;
		this.personeName = personeStatus.animal;
		this.place = placeDefine.personPlace;
		this.descriptionText = personeStatus.description;
		this.image = personeStatus.image;
		this.animalIcon = personeStatus.animalIcon;
		return this.createSingleRow(index);
	}
	createSingleRow(index){
		return `<div class="info-block">${index+1}. App said: ${this.fullPhrase} | You anima is: ${this.personeName} | ${this.place}</div> ${this.erres}`
	}
}

input3.onclick = function(){
    showResult()
}
input4.onchange = function(){
	searchForm(this.value)
}

import '../css/style.css'
import '../css/secondStyle.css'

function importAll(obj){
	return obj.keys().map(obj)
}
const images = importAll(require.context('../img/', false, /\.(png|jpe?g|svg)$/));
// Greatings / name / persone / place
